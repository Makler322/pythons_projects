import os
from tkinter import Tk, Button, StringVar, OptionMenu, Text, WORD, END
from tkinter.filedialog import askdirectory
from collections import defaultdict

root = Tk()

dict_settings = {
    'directory': None,
}


def get_list_files(folder):
    files = []
    ext = extention_variable.get()
    for root, dirnames, filenames in os.walk(folder):
        for filename in filenames:
            if filename[-len(ext):] == ext:
                # rootn = os.path.join(*os.path.split(root))
                files.append(os.path.join(root, filename))
    return files


def askopen(event):
    dict_settings["directory"] = askdirectory()


def start(event):
    if dict_settings["directory"] is not None:
        files = get_list_files(dict_settings["directory"])

        result_dict = defaultdict(list)
        result2_dict = defaultdict(list)
        for file in files:
            result_dict[os.path.getsize(file)].append(file)

        for size, files in result_dict.items():
            if len(files) > 1:
                for file in files:
                    result2_dict[hash(open(file, 'rb').read())].append(file)
            else:
                result2_dict[size] = result_dict[size]

        result_list = []
        for key, files in result2_dict.items():
            if len(files) > 1:
                for file in files:
                    result_list.append(file)
                result_list.append('\n')
        result_text.delete('1.0', END)
        result_text.insert('1.0', "\n".join(result_list))


extentions = (".pdf", ".djvu")

extention_variable = StringVar(root)
extention_variable.set(extentions[0])

params_btn = {
    'width': 30,
    'height': 5,
    'bg': "white",
    'fg': "black",
}

result_text = Text(root, width=40,
                   font="Verdana 12",
                   wrap=WORD)

extention_menu = OptionMenu(root,
                            extention_variable, *extentions)

btn_select_folder = Button(root,
                           text="Выберите папку", **params_btn)

btn_start = Button(root,
                   text="Искать", **params_btn)

btn_select_folder.bind("<Button-1>", askopen)
btn_start.bind("<Button-1>", start)

btn_select_folder.pack()
extention_menu.pack()
btn_start.pack()
result_text.pack()

root.mainloop()