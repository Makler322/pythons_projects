from threading import Thread
from PIL import Image #Импортируем библиотеку PIL
import queue
import pathlib


def inputer():  # Функция инпутера, котрая считывает данные и кладёт их очередь инпут,
                # если введено любое слово функция завершает свою работу, положив в очередь "Stop"
    while True:
        A = file.readline().split()
        if len(A) < 3:
            for i in range(N):
                q_input.put([None, 'Stop', None])
            break
        else:
            URL, Move, Name = A
            q_input.put([URL, Move, Name]) # Данные разбиваются на 3 состовляющих: Адрес, Операция, Владельца фото


def outputer(Stop_count, Count, N):  # Функция оутпутера которая кладёт готовое изображение в нужную папку,
                 # но если встречаем "Stop" меняем счётчик на +1

    while True:
        [S, img, a, b] = q_output.get()
        #Count += 1
        if S == 'Vasya':
            pathlib.Path('./Vasya').mkdir(parents=True, exist_ok=True)    # Создание папки если её ещё нет
            img.save('Vasya/{}.jpg'.format(str(a[a.rfind("Images") + len("Images"):a.rfind(".")]) + '_' + str(b)))
            # Кладём изображение c преждним именем и операцией, которую сделали
        elif S == 'Petya':
            pathlib.Path('./Petya').mkdir(parents=True, exist_ok=True)
            img.save('Petya/{}.jpg'.format(str(a[a.rfind("Images") + len("Images"):a.rfind(".")]) + '_' + str(b)))
        elif S == 'Varya':
            pathlib.Path('./Varya').mkdir(parents=True, exist_ok=True)
            img.save('Varya/{}.jpg'.format(str(a[a.rfind("Images") + len("Images"):a.rfind(".")]) + '_' + str(b)))
        elif S == 'Stop':  # Если счетчик сравнялся с кол-во вореров - прекращаем работу программы
            Stop_count += 1
        if Stop_count == N:
            break

def worker():  # Функция рабочего, которая достаёт данные из очереди инпут, инициализирует операцию,
               # выполняет её и кладёт результат в очередь оутпут
    while True:
        [a, b, c] = q_input.get()

        if b == 'WhtBlck':
            img = Image.open(str(a)).convert('RGB')  # Берём и открываем фото
            h, l = img.size  # Высота и длина
            for i in range(h):  # Пробегаемся по высоте и длине
                for j in range(l):
                    RGB = img.getpixel((i, j))
                    r = RGB[0]  # Узнаем параметры красного, зеленого  и голубого
                    g = RGB[1]
                    b = RGB[2]
                    img.putpixel((i, j),
                    ((r + g + b) // 3, (r + g + b) // 3, (r + g + b) // 3))  # Данному пикселю задаю среднее арифметическое значение всех трех параметров
            q_output.put([c, img, a, 'WhtBlck'])
        elif b == 'Invrs':
            img = Image.open(str(a)).convert('RGB')  # Берём и открываем фото
            h, l = img.size  # Высота и длина
            for i in range(h):  # Пробегаемся по высоте и длине
                for j in range(l):
                    RGB = img.getpixel((i, j))
                    r = RGB[0]  # Узнаем параметры
                    g = RGB[1]
                    b = RGB[2]
                    img.putpixel((i, j), (b, g, r))  # Меняем местами параметры
            q_output.put([c, img, a, 'Invrs'])
        elif b == 'Stop':  # Если операция подразумевает выход, то рабочий прекращает свою работу
            q_output.put([b, None, a, b])
            break

if __name__ == '__main__':
    N = 3
    Count = 0
    Stop_count = 0
    file = open('input.txt', 'r')  # Открываем файлик с путями для чтения изображений
    q_output = queue.Queue()
    q_input = queue.Queue()

    T2 = [None]*N  # Создаем массив воркеров

    t1 = Thread(target=inputer)  # Начинаем работу с потоками
    for i in range(N):
        T2[i] = Thread(target=worker)
    t3 = Thread(target=outputer, args= (Stop_count, Count, N))

    t1.start()  # Запускаем все наши функции
    for i in range(N):
        T2[i].start()
    t3.start()


    t1.join()  # Прекращяем работу
    for i in range(N):
        T2[i].join()
    t3.join()

