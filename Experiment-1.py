import matplotlib.pyplot as plt

X=[] #Массив времени (координта х)
Y=[]
Yex=[82, 80.5, 79.5, 78.0, 77.0, 76.5, 75.5, 75.0, 74.0, 73.0, 72.5, 72.5, 72, 71.5, 71.0, 70.0, 70.0, 69.0, 69.0, 68.0, 68.0, 67.0, 66.5, 66.0, 66.0, 65.5, 65.0, 64.5, 64.0, 63.0] #Ручками вводим эксперементальные данные
Tstart=Yex[0]  #Начальная температура кофе
Tk=26 #Комнатная температура
dT=0.5 #Шаг времени
r=0 #Коэффициент r

def eiler(r,Tnew,i): #Алгоритм Эйлера
    Tnew=Tnew-r*(Tnew-Tk)*dT
    return (Yex[i]-Tnew)**2, Tnew

for i in range(10000):
    r+=0.0001 #Прибавляем к коэффициенту шаг
    k=0
    Tstart=Yex[0] #Анулирование начальной переменной

    for j in range(1,len(Yex)): #Запускаем цикл в котором запускаем эйлера
        k+=eiler(r,Tstart,j)[0]
        Tstart=eiler(r,Tstart,j)[1]

    X.append(r) #После цикла добавляем r
    Y.append(k/(j-1)) #А так же результат программы после цикла

graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Coefficient r')
plt.ylabel('Standard deviation')
ax.plot(X,Y)
j=min(Y)
for i in range(len(Y)):
    if Y[i]==j:
        print(X[i])
        break #Выводим минимальное r
plt.show()