import matplotlib.pyplot as plt
A = [100, 50, 100, 100, 150, 100, 125]
B = [5, 10, 10, 4, 4, 3, 4]
C = [0.582080, 0.578278, 0.581957, 0.582611, 0.581548, 0.578524, 0.581548]
D = [0,0,0,0.5885125273656044,0.5904585259061056, 0.584681342738993, 0.5907321819508635]
E = [0,0,0,0.5796296296296296,0.5804320987654321, 0.5786419753086419, 0.5802469135802469]
for i in range(len(D)):
 print(C[i], abs(D[i] - E[i]))
'''
max_depth: 4 n_estimators 80|||Train Accuracy: 0.5880260277304792|||Validation Accuracy: 0.5804938271604938
max_depth: 4 n_estimators 81|||Train Accuracy: 0.5880868401848699|||Validation Accuracy: 0.5812345679012346
max_depth: 4 n_estimators 82|||Train Accuracy: 0.5879044028216979|||Validation Accuracy: 0.5811111111111111
max_depth: 4 n_estimators 83|||Train Accuracy: 0.5877523716857213|||Validation Accuracy: 0.5808641975308642
max_depth: 4 n_estimators 84|||Train Accuracy: 0.587721965458526|||Validation Accuracy: 0.5814197530864198
max_depth: 4 n_estimators 85|||Train Accuracy: 0.5881476526392605|||Validation Accuracy: 0.5806172839506173
max_depth: 4 n_estimators 86|||Train Accuracy: 0.5888165896375578|||Validation Accuracy: 0.5812962962962963
max_depth: 4 n_estimators 87|||Train Accuracy: 0.5888774020919484|||Validation Accuracy: 0.5811728395061728
max_depth: 4 n_estimators 88|||Train Accuracy: 0.5885125273656044|||Validation Accuracy: 0.5812962962962963
max_depth: 4 n_estimators 89|||Train Accuracy: 0.5885125273656044|||Validation Accuracy: 0.581604938271605
max_depth: 4 n_estimators 90|||Train Accuracy: 0.5883300900024325|||Validation Accuracy: 0.580925925925926
max_depth: 4 n_estimators 91|||Train Accuracy: 0.5879956215032839|||Validation Accuracy: 0.5800617283950618
max_depth: 4 n_estimators 92|||Train Accuracy: 0.5878739965945026|||Validation Accuracy: 0.5800617283950618
max_depth: 4 n_estimators 93|||Train Accuracy: 0.5882996837752371|||Validation Accuracy: 0.5804320987654321
max_depth: 4 n_estimators 94|||Train Accuracy: 0.5882996837752371|||Validation Accuracy: 0.5800617283950618
max_depth: 4 n_estimators 95|||Train Accuracy: 0.5883604962296278|||Validation Accuracy: 0.5800617283950618
max_depth: 4 n_estimators 96|||Train Accuracy: 0.5885733398199952|||Validation Accuracy: 0.5796913580246914
max_depth: 4 n_estimators 97|||Train Accuracy: 0.5887557771831671|||Validation Accuracy: 0.5796296296296296
max_depth: 4 n_estimators 98|||Train Accuracy: 0.5884821211384091|||Validation Accuracy: 0.5798765432098766
max_depth: 4 n_estimators 99|||Train Accuracy: 0.5888469958647531|||Validation Accuracy: 0.5796296296296296
max_depth: 4 n_estimators 100|||Train Accuracy: 0.5885125273656044|||Validation Accuracy: 0.5796296296296296
max_depth: 4 n_estimators 101|||Train Accuracy: 0.5888469958647531|||Validation Accuracy: 0.5791975308641976
max_depth: 4 n_estimators 102|||Train Accuracy: 0.5887253709559718|||Validation Accuracy: 0.5794444444444444
max_depth: 4 n_estimators 103|||Train Accuracy: 0.5884213086840185|||Validation Accuracy: 0.5788271604938272
max_depth: 4 n_estimators 104|||Train Accuracy: 0.5884213086840185|||Validation Accuracy: 0.5790740740740741
max_depth: 4 n_estimators 105|||Train Accuracy: 0.5888774020919484|||Validation Accuracy: 0.5790740740740741
max_depth: 4 n_estimators 106|||Train Accuracy: 0.5889078083191438|||Validation Accuracy: 0.5786419753086419
max_depth: 4 n_estimators 107|||Train Accuracy: 0.589120651909511|||Validation Accuracy: 0.578395061728395
max_depth: 4 n_estimators 108|||Train Accuracy: 0.5888774020919484|||Validation Accuracy: 0.5788888888888889
max_depth: 4 n_estimators 109|||Train Accuracy: 0.5889686207735344|||Validation Accuracy: 0.5791358024691358
max_depth: 4 n_estimators 110|||Train Accuracy: 0.5889990270007297|||Validation Accuracy: 0.5792592592592593
max_depth: 4 n_estimators 111|||Train Accuracy: 0.5893639017270738|||Validation Accuracy: 0.5790123456790124
max_depth: 4 n_estimators 112|||Train Accuracy: 0.5893334954998783|||Validation Accuracy: 0.5791358024691358
max_depth: 4 n_estimators 113|||Train Accuracy: 0.589303089272683|||Validation Accuracy: 0.5793827160493827
max_depth: 4 n_estimators 114|||Train Accuracy: 0.5897287764534177|||Validation Accuracy: 0.5795061728395061
max_depth: 4 n_estimators 115|||Train Accuracy: 0.5896983702262223|||Validation Accuracy: 0.5797530864197531
max_depth: 4 n_estimators 116|||Train Accuracy: 0.589667963999027|||Validation Accuracy: 0.5798148148148148
max_depth: 4 n_estimators 117|||Train Accuracy: 0.5900328387253709|||Validation Accuracy: 0.5800617283950618
max_depth: 4 n_estimators 118|||Train Accuracy: 0.5903673072245196|||Validation Accuracy: 0.5801851851851851
max_depth: 4 n_estimators 119|||Train Accuracy: 0.5902760885429336|||Validation Accuracy: 0.5799382716049383
max_depth: 4 n_estimators 120|||Train Accuracy: 0.590215276088543|||Validation Accuracy: 0.5799382716049383
max_depth: 4 n_estimators 121|||Train Accuracy: 0.5905193383604962|||Validation Accuracy: 0.5799382716049383
max_depth: 4 n_estimators 122|||Train Accuracy: 0.5905497445876916|||Validation Accuracy: 0.5803703703703704
max_depth: 4 n_estimators 123|||Train Accuracy: 0.590306494770129|||Validation Accuracy: 0.5801234567901234
max_depth: 4 n_estimators 124|||Train Accuracy: 0.5902760885429336|||Validation Accuracy: 0.5803086419753086
max_depth: 4 n_estimators 125|||Train Accuracy: 0.5907321819508635|||Validation Accuracy: 0.5802469135802469
max_depth: 4 n_estimators 126|||Train Accuracy: 0.5909450255412309|||Validation Accuracy: 0.5804320987654321
max_depth: 4 n_estimators 127|||Train Accuracy: 0.5905801508148869|||Validation Accuracy: 0.5806172839506173
max_depth: 4 n_estimators 128|||Train Accuracy: 0.5905193383604962|||Validation Accuracy: 0.5796296296296296
max_depth: 4 n_estimators 129|||Train Accuracy: 0.5904281196789103|||Validation Accuracy: 0.5802469135802469
max_depth: 4 n_estimators 130|||Train Accuracy: 0.5902760885429336|||Validation Accuracy: 0.5798765432098766
max_depth: 4 n_estimators 131|||Train Accuracy: 0.590215276088543|||Validation Accuracy: 0.5801851851851851
max_depth: 4 n_estimators 132|||Train Accuracy: 0.5904889321333009|||Validation Accuracy: 0.5801234567901234
max_depth: 4 n_estimators 133|||Train Accuracy: 0.5899416200437849|||Validation Accuracy: 0.5803086419753086
max_depth: 4 n_estimators 134|||Train Accuracy: 0.5900024324981756|||Validation Accuracy: 0.5800617283950618
max_depth: 4 n_estimators 135|||Train Accuracy: 0.5900024324981756|||Validation Accuracy: 0.5796296296296296
max_depth: 4 n_estimators 136|||Train Accuracy: 0.5893943079542691|||Validation Accuracy: 0.5798765432098766
max_depth: 4 n_estimators 137|||Train Accuracy: 0.5895159328630504|||Validation Accuracy: 0.5793209876543209
max_depth: 4 n_estimators 138|||Train Accuracy: 0.589759182680613|||Validation Accuracy: 0.5793827160493827
max_depth: 4 n_estimators 139|||Train Accuracy: 0.5899720262709803|||Validation Accuracy: 0.5793209876543209
max_depth: 4 n_estimators 140|||Train Accuracy: 0.590124057406957|||Validation Accuracy: 0.5796913580246914
max_depth: 4 n_estimators 141|||Train Accuracy: 0.5902456823157383|||Validation Accuracy: 0.5795679012345679

'''