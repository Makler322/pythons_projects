import pandas as pd
import matplotlib.pyplot as plt
table = pd.concat([
    pd.read_csv('train.csv', index_col=0),
    pd.read_csv('target.csv', index_col=0)
], axis=1)
table['radiant_gold'] = table['r1_gold'] + table['r2_gold'] + table['r3_gold'] + table['r4_gold'] + table['r5_gold']
table['dire_gold'] = table['d1_gold'] + table['d2_gold'] + table['d3_gold'] + table['d4_gold'] + table['d5_gold']
table['gold_diff'] = table['radiant_gold'] - table['dire_gold']
table['radiant_xp'] = table['r1_xp'] + table['r2_xp'] + table['r3_xp'] + table['r4_xp'] + table['r5_xp']
table['dire_xp'] = table['d1_xp'] + table['d2_xp'] + table['d3_xp'] + table['d4_xp'] + table['d5_xp']
#table.hist('radiant_gold', bins=50)
table_truncated = table
#table_truncated.groupby('radiant_won')['gold_diff'].hist(alpha=0.5, bins=50)
#plt.legend(table_truncated['radiant_won'].unique())
#plt.show()
#print(table_truncated.groupby('radiant_won')['gold_diff'].mean())
#table_truncated.plot(kind='scatter', x='radiant_xp', y='radiant_gold')
plt.figure(figsize=(5, 5))
plt.scatter(table_truncated['dire_xp'], table_truncated['dire_gold'])
table_truncated2 = table_truncated.query(' dire_gold > 250000 ')
print(table_truncated2)
plt.show()
# 4689