import pandas as pd
import matplotlib.pyplot as plt
import sklearn
import math
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

if __name__ == '__main__':

    x = pd.read_csv('train.csv', index_col=0)
    y = pd.read_csv('target.csv', index_col=0)['radiant_won']
    y.value_counts()
    count_missings = x.isnull().sum()
    missings_threshold = x.shape[0] // 2
    drop_list = count_missings[count_missings > missings_threshold].index.tolist()

    x.drop(drop_list, axis=1, inplace=True)
    x = x.fillna(0)

    categorical = [
        'r1_hero', 'r2_hero', 'r3_hero', 'r4_hero', 'r5_hero',
        'd1_hero', 'd2_hero', 'd3_hero', 'd4_hero', 'd5_hero'
    ]

    hero_dummies = pd.get_dummies(x[categorical[0]])
    for col in categorical[1:]:
        hero_dummies = hero_dummies + pd.get_dummies(x[col])
    #print(hero_dummies)
    hero_dummies.columns = ['hero_{}'.format(col_name) for col_name in hero_dummies.columns]
    x.drop(categorical, axis=1, inplace=True)
    x = pd.concat([x, hero_dummies], axis=1)


    x_train, x_validation, y_train, y_validation = train_test_split(x, y, test_size=.33, random_state=1)


    param_grid = {'n_estimators': [10, 50, 100], 'max_depth': [5, 10, 15]}
    clf = GridSearchCV(RandomForestClassifier(random_state=322), param_grid, verbose=3, n_jobs=-1)
    clf.fit(x_train, y_train)
    best_clf = clf.best_estimator_


    train_acc = accuracy_score(y_train, best_clf.predict(x_train))
    validation_acc = accuracy_score(y_validation, best_clf.predict(x_validation))
    print('Train Accuracy:', train_acc)
    print('Validation Accuracy:', validation_acc)


    '''
322
Train Accuracy: 0.5943809292143031
Validation Accuracy: 0.5819135802469135
300
Train Accuracy: 0.6113171977621017
Validation Accuracy: 0.5818518518518518
300 (better)
Train Accuracy: 0.6124726343955242
Validation Accuracy: 0.5824691358024692

322(better)
Train Accuracy: 0.5956883969837022
Validation Accuracy: 0.5825925925925926
    '''