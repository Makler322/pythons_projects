import pandas  # при помощи нее мы считаем из файла таблицу с данными
#from sklearn import tree  # библиотека для машинного обучения,
# из нее нам понадобится модуль tree для работы с решающими деревьями
table = pandas.read_csv('weather.csv')
selected = table.query('wind_direction >= 0 & label == "rain" & wind_direction <= 200')
print(selected['temperature'].mean())