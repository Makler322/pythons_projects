import pandas as pd
table = pd.concat([
    pd.read_csv('submissions/1522485128.csv', index_col=0),
    pd.read_csv('submissions/1522486866.csv', index_col=0),
    pd.read_csv('submissions/1522487172.csv', index_col=0),
    pd.read_csv('submissions/1522487341.csv', index_col=0),
    pd.read_csv('submissions/1522487486.csv', index_col=0),
    pd.read_csv('submissions/1522572545.csv', index_col=0),
    pd.read_csv('submissions/1522572977.csv', index_col=0),
    pd.read_csv('submissions/1522574171.csv', index_col=0),
    pd.read_csv('submissions/1522583725.csv', index_col=0),
    pd.read_csv('submissions/1522583936.csv', index_col=0),
    pd.read_csv('submissions/1522584139.csv', index_col=0),
    pd.read_csv('submissions/1522586373.csv', index_col=0),
    pd.read_csv('submissions/1522591463.csv', index_col=0),
    pd.read_csv('submissions/1522591619.csv', index_col=0),
    pd.read_csv('submissions/1522591744.csv', index_col=0),
    pd.read_csv('submissions/1522613648.csv', index_col=0),
], axis=1)
#print(table.head())
table['radiant_wonznik'] = \
table['radiant_won1'] + \
table['radiant_won2'] + \
table['radiant_won3'] + \
table['radiant_won4'] + \
table['radiant_won5'] + \
table['radiant_won6'] + \
table['radiant_won7'] + \
table['radiant_won8'] + \
table['radiant_won9'] + \
table['radiant_won10'] + \
table['radiant_won11'] + \
table['radiant_won12'] + \
table['radiant_won13'] + \
table['radiant_won14'] + \
table['radiant_won15'] + \
table['radiant_won16']

def f(table):
    if table['radiant_wonznik'] > 8:
        return 1
    else:
        return 0
table['radiant_won'] = table.apply(f, axis=1)
#print(table.head())
table2 = table[['radiant_won']].copy()

submission_path = 'submissions/Win1.csv'
table2.to_csv(submission_path, index=True)