from IPython.display import display
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import pandas as pd
import matplotlib.pyplot as plt
import time
import os
import sklearn

x = pd.read_csv('train.csv', index_col=0)
y = pd.read_csv('target.csv', index_col=0)['radiant_won']

#print(x.shape)
#print(y.value_counts())

columns_with_single_value = [col for col in x.columns if x[col].unique().shape[0] == 1]
x = x.drop(columns_with_single_value, axis=1)

def gold(x):
    return (x['radiant_gold'] - x['dire_gold']) * 1.5

x['radiant_gold'] = x['r1_gold'] + x['r2_gold'] + x['r3_gold'] + x['r4_gold'] + x['r5_gold']
x['dire_gold'] = x['d1_gold'] + x['d2_gold'] + x['d3_gold'] + x['d4_gold'] + x['d5_gold']
x['gold_diff'] = x.apply(gold, axis=1)

def xp(x):
    return (x['radiant_xp'] - x['dire_xp']) * 1

x['radiant_xp'] = x['r1_xp'] + x['r2_xp'] + x['r3_xp'] + x['r4_xp'] + x['r5_xp']
x['dire_xp'] = x['d1_xp'] + x['d2_xp'] + x['d3_xp'] + x['d4_xp'] + x['d5_xp']
x['xp_diff'] = x.apply(xp, axis=1)

def kills(x):
    return (x['radiant_kills'] - x['dire_kills']) * 200

x['radiant_kills'] = x['r1_kills'] + x['r2_kills'] + x['r3_kills'] + x['r4_kills'] + x['r5_kills']
x['dire_kills'] = x['d1_kills'] + x['d2_kills'] + x['d3_kills'] + x['d4_kills'] + x['d5_kills']
x['kills_diff'] = x.apply(kills, axis=1)

def lh(x):
    return (x['radiant_lh'] - x['dire_lh']) * 37.5

x['radiant_lh'] = x['r1_lh'] + x['r2_lh'] + x['r3_lh'] + x['r4_lh'] + x['r5_lh']
x['dire_lh'] = x['d1_lh'] + x['d2_lh'] + x['d3_lh'] + x['d4_lh'] + x['d5_lh']
x['lh_diff'] = x.apply(lh, axis=1)

def ward_observer_count(x):
    return (x['radiant_ward_observer_count'] - x['dire_ward_observer_count']) * 100

x['observer_count_diff'] = x.apply(ward_observer_count, axis=1)

def ward_sentry_count(x):
    return (x['radiant_ward_sentry_count'] - x['dire_ward_sentry_count']) * 80

x['sentry_count_diff'] = x.apply(ward_sentry_count, axis=1)

x.drop(['radiant_gold', 'dire_gold'], axis=1, inplace=True)
x.drop(['radiant_xp', 'dire_xp'], axis=1, inplace=True)
x.drop(['radiant_kills', 'dire_kills'], axis=1, inplace=True)
x.drop(['radiant_lh', 'dire_lh'], axis=1, inplace=True)

#x_find  = x[['gold_diff', 'xp_diff', 'kills_diff', 'lh_diff', 'observer_count_diff', 'sentry_count_diff']].copy()

x_train, x_validation, y_train, y_validation = train_test_split(x, y, test_size=.33, random_state=322)
x_train = x_train.fillna(0)
x_validation = x_validation.fillna(0)

scaler = MinMaxScaler()
x_train = scaler.fit_transform(x_train)
x_validation = scaler.fit_transform(x_validation)
features = list(x.columns)


clf = RandomForestClassifier(n_estimators=100, max_depth=4, random_state=322)
clf.fit(x_train, y_train)

k = accuracy_score(y_train, clf.predict(x_train))
l = accuracy_score(y_validation, clf.predict(x_validation))
print()
print('Train Accuracy:', k,)
print('Validation Accuracy:', l)
print('----------------------------')

x_test = pd.read_csv('test.csv', index_col=0)
y_submission = pd.read_csv('submission.csv', index_col=0)

def gold(x_test):
    return (x_test['radiant_gold'] - x_test['dire_gold']) * 1.5

x_test['radiant_gold'] = x_test['r1_gold'] + x_test['r2_gold'] + x_test['r3_gold'] + x_test['r4_gold'] + x_test['r5_gold']
x_test['dire_gold'] = x_test['d1_gold'] + x_test['d2_gold'] + x_test['d3_gold'] + x_test['d4_gold'] + x_test['d5_gold']
x_test['gold_diff'] = x_test.apply(gold, axis=1)

def xp(x_test):
    return (x_test['radiant_xp'] - x_test['dire_xp']) * 1

x_test['radiant_xp'] = x_test['r1_xp'] + x_test['r2_xp'] + x_test['r3_xp'] + x_test['r4_xp'] + x_test['r5_xp']
x_test['dire_xp'] = x_test['d1_xp'] + x_test['d2_xp'] + x_test['d3_xp'] + x_test['d4_xp'] + x_test['d5_xp']
x_test['xp_diff'] = x_test.apply(xp, axis=1)

def kills(x_test):
    return (x_test['radiant_kills'] - x_test['dire_kills']) * 200

x_test['radiant_kills'] = x_test['r1_kills'] + x_test['r2_kills'] + x_test['r3_kills'] + x_test['r4_kills'] + x_test['r5_kills']
x_test['dire_kills'] = x_test['d1_kills'] + x_test['d2_kills'] + x_test['d3_kills'] + x_test['d4_kills'] + x_test['d5_kills']
x_test['kills_diff'] = x_test.apply(kills, axis=1)

def lh(x_test):
    return (x_test['radiant_lh'] - x_test['dire_lh']) * 37.5

x_test['radiant_lh'] = x_test['r1_lh'] + x_test['r2_lh'] + x_test['r3_lh'] + x_test['r4_lh'] + x_test['r5_lh']
x_test['dire_lh'] = x_test['d1_lh'] + x_test['d2_lh'] + x_test['d3_lh'] + x_test['d4_lh'] + x_test['d5_lh']
x_test['lh_diff'] = x_test.apply(lh, axis=1)

def ward_observer_count(x_test):
    return (x_test['radiant_ward_observer_count'] - x_test['dire_ward_observer_count']) * 100

x_test['observer_count_diff'] = x_test.apply(ward_observer_count, axis=1)

def ward_sentry_count(x_test):
    return (x_test['radiant_ward_sentry_count'] - x_test['dire_ward_sentry_count']) * 80

x_test['sentry_count_diff'] = x_test.apply(ward_sentry_count, axis=1)

print(1)
x_test = x_test.fillna(0)
print(2)
x_test = x_test[features]
print(3)
scaler = MinMaxScaler()
print(4)
x_test = scaler.fit_transform(x_test)
print(5)
y_submission['radiant_won'] = clf.predict(x_test)
print(6)
current_timestamp = int(time.time())
print(7)
submission_path = 'submissions/{}.csv'.format(current_timestamp)
print(8)
if not os.path.exists('submissions'):
    os.makedirs('submissions')

print(submission_path)
y_submission.to_csv(submission_path, index=True)

'''

70 2
Train Accuracy: 0.5720627584529312
Validation Accuracy: 0.5687654320987654
----------------------------
70 3
Train Accuracy: 0.5862928727803454
Validation Accuracy: 0.5766666666666667
----------------------------
70 4
Train Accuracy: 0.5896983702262223
Validation Accuracy: 0.5801234567901234
----------------------------
70 5
Train Accuracy: 0.5971174896618827
Validation Accuracy: 0.5824691358024692
----------------------------
70 6
Train Accuracy: 0.6122597908051569
Validation Accuracy: 0.5822222222222222
----------------------------
70 7
Train Accuracy: 0.6311420578934566
Validation Accuracy: 0.5848765432098766
----------------------------
70 8
Train Accuracy: 0.6617915349063488
Validation Accuracy: 0.5824691358024692
----------------------------
70 9
Train Accuracy: 0.6970627584529312
Validation Accuracy: 0.5837654320987654
----------------------------
70 10
Train Accuracy: 0.744830941376794
Validation Accuracy: 0.581358024691358
----------------------------
70 11
Train Accuracy: 0.7933288737533447
Validation Accuracy: 0.581604938271605
----------------------------
70 12
Train Accuracy: 0.8424957431281926
Validation Accuracy: 0.5793827160493827
----------------------------
70 13
Train Accuracy: 0.8926356117732912
Validation Accuracy: 0.5765432098765432
----------------------------
72 2
Train Accuracy: 0.5714242276818292
Validation Accuracy: 0.5684567901234568
----------------------------
72 3
Train Accuracy: 0.5855631233276575
Validation Accuracy: 0.5766666666666667
----------------------------
72 4
Train Accuracy: 0.5900328387253709
Validation Accuracy: 0.5812345679012346
----------------------------
72 5
Train Accuracy: 0.5967222087083435
Validation Accuracy: 0.5829012345679012
----------------------------
72 6
Train Accuracy: 0.6109523230357577
Validation Accuracy: 0.581604938271605
----------------------------
72 7
Train Accuracy: 0.6316589637557772
Validation Accuracy: 0.5839506172839506
----------------------------
72 8
Train Accuracy: 0.6601191924106057
Validation Accuracy: 0.5827777777777777
----------------------------
72 9
Train Accuracy: 0.695937728046704
Validation Accuracy: 0.5849382716049383
----------------------------
72 10
Train Accuracy: 0.74492216005838
Validation Accuracy: 0.5811728395061728
----------------------------
72 11
Train Accuracy: 0.7952140598394551
Validation Accuracy: 0.582037037037037
----------------------------
72 12
Train Accuracy: 0.8430126489905132
Validation Accuracy: 0.5804320987654321
----------------------------
72 13
Train Accuracy: 0.8929092678180491
Validation Accuracy: 0.5762345679012346
----------------------------
74 2
Train Accuracy: 0.5734614449039164
Validation Accuracy: 0.57
----------------------------
74 3
Train Accuracy: 0.5856543420092435
Validation Accuracy: 0.5772222222222222
----------------------------
74 4
Train Accuracy: 0.589303089272683
Validation Accuracy: 0.5811728395061728
----------------------------
74 5
Train Accuracy: 0.5964485526635855
Validation Accuracy: 0.5830246913580247
----------------------------
74 6
Train Accuracy: 0.6105570420822184
Validation Accuracy: 0.5823456790123457
----------------------------
74 7
Train Accuracy: 0.63099002675748
Validation Accuracy: 0.5835802469135802
----------------------------
74 8
Train Accuracy: 0.6587205059596205
Validation Accuracy: 0.5830864197530864
----------------------------
74 9
Train Accuracy: 0.6940525419605935
Validation Accuracy: 0.5838888888888889
----------------------------
74 10
Train Accuracy: 0.744374847968864
Validation Accuracy: 0.5801851851851851
----------------------------
74 11
Train Accuracy: 0.7941194356604232
Validation Accuracy: 0.5819753086419753
----------------------------
74 12
Train Accuracy: 0.8419788372658721
Validation Accuracy: 0.5796913580246914
----------------------------
74 13
Train Accuracy: 0.8930308927268304
Validation Accuracy: 0.5758024691358025
----------------------------
76 2
Train Accuracy: 0.5740087569934322
Validation Accuracy: 0.5697530864197531
----------------------------
76 3
Train Accuracy: 0.5863536852347361
Validation Accuracy: 0.5767901234567901
----------------------------
76 4
Train Accuracy: 0.5882692775480418
Validation Accuracy: 0.5811728395061728
----------------------------
76 5
Train Accuracy: 0.5961140841644369
Validation Accuracy: 0.5837037037037037
----------------------------
76 6
Train Accuracy: 0.6102529798102652
Validation Accuracy: 0.5815432098765432
----------------------------
76 7
Train Accuracy: 0.6305643395767453
Validation Accuracy: 0.5841358024691358
----------------------------
76 8
Train Accuracy: 0.6587813184140112
Validation Accuracy: 0.5829012345679012
----------------------------
76 9
Train Accuracy: 0.6939613232790075
Validation Accuracy: 0.5845679012345679
----------------------------
76 10
Train Accuracy: 0.7460775966918025
Validation Accuracy: 0.5814814814814815
----------------------------
76 11
Train Accuracy: 0.793724154706884
Validation Accuracy: 0.5826543209876544
----------------------------
76 12
Train Accuracy: 0.8427693991729506
Validation Accuracy: 0.5798148148148148
----------------------------
76 13
Train Accuracy: 0.894095110678667
Validation Accuracy: 0.5765432098765432
----------------------------
78 2
Train Accuracy: 0.5742824130381903
Validation Accuracy: 0.5711728395061728
----------------------------
78 3
Train Accuracy: 0.5859279980540014
Validation Accuracy: 0.5782098765432099
----------------------------
78 4
Train Accuracy: 0.587721965458526
Validation Accuracy: 0.5810493827160493
----------------------------
78 5
Train Accuracy: 0.594654585259061
Validation Accuracy: 0.5831481481481482
----------------------------
78 6
Train Accuracy: 0.6101617611286791
Validation Accuracy: 0.5820987654320988
----------------------------
78 7
Train Accuracy: 0.629986621260034
Validation Accuracy: 0.5838888888888889
----------------------------
78 8
Train Accuracy: 0.6576866942349793
Validation Accuracy: 0.5831481481481482
----------------------------
78 9
Train Accuracy: 0.693170761371929
Validation Accuracy: 0.5845679012345679
----------------------------
78 10
Train Accuracy: 0.7441315981513014
Validation Accuracy: 0.5826543209876544
----------------------------
78 11
Train Accuracy: 0.7924166869374848
Validation Accuracy: 0.5823456790123457
----------------------------
78 12
Train Accuracy: 0.8431950863536852
Validation Accuracy: 0.5802469135802469
----------------------------
78 13
Train Accuracy: 0.8955850158112382
Validation Accuracy: 0.5780864197530864
----------------------------
80 2
Train Accuracy: 0.5758331306251521
Validation Accuracy: 0.5725308641975309
----------------------------
80 3
Train Accuracy: 0.5859584042811968
Validation Accuracy: 0.578395061728395
----------------------------
80 4
Train Accuracy: 0.5880260277304792
Validation Accuracy: 0.5804938271604938
----------------------------
80 5
Train Accuracy: 0.5948066163950377
Validation Accuracy: 0.5833333333333334
----------------------------
80 6
Train Accuracy: 0.608671855996108
Validation Accuracy: 0.582283950617284
----------------------------
80 7
Train Accuracy: 0.6308075893943079
Validation Accuracy: 0.5845679012345679
----------------------------
80 8
Train Accuracy: 0.657382631963026
Validation Accuracy: 0.5829012345679012
----------------------------
80 9
Train Accuracy: 0.6926842617368038
Validation Accuracy: 0.5839506172839506
----------------------------
80 10
Train Accuracy: 0.7438275358793481
Validation Accuracy: 0.5823456790123457
----------------------------
80 11
Train Accuracy: 0.7912004378496716
Validation Accuracy: 0.5819753086419753
----------------------------
80 12
Train Accuracy: 0.842070055947458
Validation Accuracy: 0.580679012345679
----------------------------
80 13
Train Accuracy: 0.8941255169058623
Validation Accuracy: 0.5767283950617283
----------------------------
82 2
Train Accuracy: 0.5761371928971053
Validation Accuracy: 0.5714197530864198
----------------------------
82 3
Train Accuracy: 0.5862016540987595
Validation Accuracy: 0.5774074074074074
----------------------------
82 4
Train Accuracy: 0.5879044028216979
Validation Accuracy: 0.5811111111111111
----------------------------
82 5
Train Accuracy: 0.5945937728046704
Validation Accuracy: 0.5837037037037037
----------------------------
82 6
Train Accuracy: 0.608580637314522
Validation Accuracy: 0.5834567901234567
----------------------------
82 7
Train Accuracy: 0.6301994648504013
Validation Accuracy: 0.5846913580246914
----------------------------
82 8
Train Accuracy: 0.6571393821454634
Validation Accuracy: 0.5825925925925926
----------------------------
82 9
Train Accuracy: 0.6921977621016784
Validation Accuracy: 0.5829012345679012
----------------------------
82 10
Train Accuracy: 0.7427633179275116
Validation Accuracy: 0.5814197530864198
----------------------------
82 11
Train Accuracy: 0.7898625638530771
Validation Accuracy: 0.582037037037037
----------------------------
82 12
Train Accuracy: 0.8419788372658721
Validation Accuracy: 0.5791358024691358
----------------------------
82 13
Train Accuracy: 0.8936998297251277
Validation Accuracy: 0.577037037037037
----------------------------
84 2
Train Accuracy: 0.5764412551690586
Validation Accuracy: 0.5721604938271605
----------------------------
84 3
Train Accuracy: 0.5858367793724155
Validation Accuracy: 0.5783333333333334
----------------------------
84 4
Train Accuracy: 0.587721965458526
Validation Accuracy: 0.5814197530864198
----------------------------
84 5
Train Accuracy: 0.5944113354414984
Validation Accuracy: 0.5824074074074074
----------------------------
84 6
Train Accuracy: 0.60839819995135
Validation Accuracy: 0.5837654320987654
----------------------------
84 7
Train Accuracy: 0.630351495986378
Validation Accuracy: 0.5848148148148148
----------------------------
84 8
Train Accuracy: 0.6578995378253466
Validation Accuracy: 0.5834567901234567
----------------------------
84 9
Train Accuracy: 0.6918632936025298
Validation Accuracy: 0.582716049382716
----------------------------
84 10
Train Accuracy: 0.7431585988810508
Validation Accuracy: 0.582716049382716
----------------------------
84 11
Train Accuracy: 0.790865969350523
Validation Accuracy: 0.581358024691358
----------------------------
84 12
Train Accuracy: 0.8418268061298954
Validation Accuracy: 0.5782098765432099
----------------------------
84 13
Train Accuracy: 0.8938822670882997
Validation Accuracy: 0.5790740740740741
----------------------------
86 2
Train Accuracy: 0.578600097299927
Validation Accuracy: 0.5741358024691358
----------------------------
86 3
Train Accuracy: 0.5851374361469229
Validation Accuracy: 0.5773456790123457
----------------------------
86 4
Train Accuracy: 0.5888165896375578
Validation Accuracy: 0.5812962962962963
----------------------------
86 5
Train Accuracy: 0.5941680856239357
Validation Accuracy: 0.582962962962963
----------------------------
86 6
Train Accuracy: 0.6075468255898808
Validation Accuracy: 0.5845061728395061
----------------------------
86 7
Train Accuracy: 0.6294089029433227
Validation Accuracy: 0.5848765432098766
----------------------------
86 8
Train Accuracy: 0.6561967891024082
Validation Accuracy: 0.5824074074074074
----------------------------
86 9
Train Accuracy: 0.6915288251033812
Validation Accuracy: 0.5843827160493827
----------------------------
86 10
Train Accuracy: 0.7426416930187303
Validation Accuracy: 0.5840740740740741
----------------------------
86 11
Train Accuracy: 0.7917173437119922
Validation Accuracy: 0.580925925925926
----------------------------
86 12
Train Accuracy: 0.8438944295791778
Validation Accuracy: 0.5773456790123457
----------------------------
86 13
Train Accuracy: 0.894095110678667
Validation Accuracy: 0.5782098765432099
----------------------------
88 2
Train Accuracy: 0.5772014108489418
Validation Accuracy: 0.572037037037037
----------------------------
88 3
Train Accuracy: 0.5850462174653369
Validation Accuracy: 0.5771604938271605
----------------------------
88 4
Train Accuracy: 0.5885125273656044
Validation Accuracy: 0.5812962962962963
----------------------------
88 5
Train Accuracy: 0.5946241790318657
Validation Accuracy: 0.5825308641975309
----------------------------
88 6
Train Accuracy: 0.6082157625881781
Validation Accuracy: 0.5837654320987654
----------------------------
88 7
Train Accuracy: 0.6298041838968621
Validation Accuracy: 0.5840740740740741
----------------------------
88 8
Train Accuracy: 0.6569873510094868
Validation Accuracy: 0.5823456790123457
----------------------------
88 9
Train Accuracy: 0.6925626368280223
Validation Accuracy: 0.5853703703703703
----------------------------
88 10
Train Accuracy: 0.744283629287278
Validation Accuracy: 0.5843827160493827
----------------------------
88 11
Train Accuracy: 0.7927511554366334
Validation Accuracy: 0.5805555555555556
----------------------------
88 12
Train Accuracy: 0.8459620530284603
Validation Accuracy: 0.5783333333333334
----------------------------
88 13
Train Accuracy: 0.8958890780831914
Validation Accuracy: 0.5773456790123457
----------------------------
90 2
Train Accuracy: 0.5790865969350523
Validation Accuracy: 0.5758641975308642
----------------------------
90 3
Train Accuracy: 0.5850766236925322
Validation Accuracy: 0.5780246913580247
----------------------------
90 4
Train Accuracy: 0.5883300900024325
Validation Accuracy: 0.580925925925926
----------------------------
90 5
Train Accuracy: 0.5943809292143031
Validation Accuracy: 0.582962962962963
----------------------------
90 6
Train Accuracy: 0.6073339819995135
Validation Accuracy: 0.5834567901234567
----------------------------
90 7
Train Accuracy: 0.6296825589880808
Validation Accuracy: 0.5835802469135802
----------------------------
90 8
Train Accuracy: 0.6558927268304549
Validation Accuracy: 0.5838271604938272
----------------------------
90 9
Train Accuracy: 0.6929579177815617
Validation Accuracy: 0.5865432098765432
----------------------------
90 10
Train Accuracy: 0.7441011919241061
Validation Accuracy: 0.5846296296296296
----------------------------
90 11
Train Accuracy: 0.7942714667963999
Validation Accuracy: 0.5804938271604938
----------------------------
90 12
Train Accuracy: 0.8463877402091948
Validation Accuracy: 0.5775308641975309
----------------------------
90 13
Train Accuracy: 0.8975918268061299
Validation Accuracy: 0.5782098765432099
----------------------------
92 2
Train Accuracy: 0.5800291899781075
Validation Accuracy: 0.5782716049382716
----------------------------
92 3
Train Accuracy: 0.5849245925565556
Validation Accuracy: 0.578395061728395
----------------------------
92 4
Train Accuracy: 0.5878739965945026
Validation Accuracy: 0.5800617283950618
----------------------------
92 5
Train Accuracy: 0.5943201167599125
Validation Accuracy: 0.5824074074074074
----------------------------
92 6
Train Accuracy: 0.6070907321819509
Validation Accuracy: 0.5837654320987654
----------------------------
92 7
Train Accuracy: 0.6299258088056434
Validation Accuracy: 0.5834567901234567
----------------------------
92 8
Train Accuracy: 0.6550413524689856
Validation Accuracy: 0.5833950617283951
----------------------------
92 9
Train Accuracy: 0.6928362928727804
Validation Accuracy: 0.585925925925926
----------------------------
92 10
Train Accuracy: 0.7430369739722695
Validation Accuracy: 0.5850617283950618
----------------------------
92 11
Train Accuracy: 0.7929335927998054
Validation Accuracy: 0.5808641975308642
----------------------------
92 12
Train Accuracy: 0.8461748966188275
Validation Accuracy: 0.5789506172839506
----------------------------
92 13
Train Accuracy: 0.8970749209438093
Validation Accuracy: 0.5799382716049383
----------------------------
94 2
Train Accuracy: 0.5803332522500608
Validation Accuracy: 0.5767901234567901
----------------------------
94 3
Train Accuracy: 0.5843772804670396
Validation Accuracy: 0.5776543209876543
----------------------------
94 4
Train Accuracy: 0.5882996837752371
Validation Accuracy: 0.5800617283950618
----------------------------
94 5
Train Accuracy: 0.5940160544879591
Validation Accuracy: 0.5828395061728395
----------------------------
94 6
Train Accuracy: 0.6073947944539042
Validation Accuracy: 0.5836419753086419
----------------------------
94 7
Train Accuracy: 0.6295305278521041
Validation Accuracy: 0.5838271604938272
----------------------------
94 8
Train Accuracy: 0.6557102894672829
Validation Accuracy: 0.5832098765432099
----------------------------
94 9
Train Accuracy: 0.6920153247385065
Validation Accuracy: 0.5871604938271605
----------------------------
94 10
Train Accuracy: 0.741607881294089
Validation Accuracy: 0.5864814814814815
----------------------------
94 11
Train Accuracy: 0.7922950620287035
Validation Accuracy: 0.5815432098765432
----------------------------
94 12
Train Accuracy: 0.8456579907565069
Validation Accuracy: 0.5798765432098766
----------------------------
94 13
Train Accuracy: 0.8964363901727074
Validation Accuracy: 0.5799382716049383
----------------------------
95 2
Train Accuracy: 0.5803940647044514
Validation Accuracy: 0.576604938271605
----------------------------
95 3
Train Accuracy: 0.5844989053758209
Validation Accuracy: 0.5782716049382716
----------------------------
95 4
Train Accuracy: 0.5883604962296278
Validation Accuracy: 0.5800617283950618
----------------------------
95 5
Train Accuracy: 0.5935295548528339
Validation Accuracy: 0.582283950617284
----------------------------
95 6
Train Accuracy: 0.6072731695451229
Validation Accuracy: 0.5836419753086419
----------------------------
95 7
Train Accuracy: 0.6292568718073461
Validation Accuracy: 0.5838271604938272
----------------------------
95 8
Train Accuracy: 0.6562271953296035
Validation Accuracy: 0.5830246913580247
----------------------------
95 9
Train Accuracy: 0.6916808562393578
Validation Accuracy: 0.586604938271605
----------------------------
95 10
Train Accuracy: 0.7422160058379956
Validation Accuracy: 0.5859876543209876
----------------------------
95 11
Train Accuracy: 0.7919909997567501
Validation Accuracy: 0.582037037037037
----------------------------
95 12
Train Accuracy: 0.8451714911213817
Validation Accuracy: 0.5806172839506173
----------------------------
95 13
Train Accuracy: 0.8968012648990513
Validation Accuracy: 0.5797530864197531
----------------------------
96 2
Train Accuracy: 0.5800291899781075
Validation Accuracy: 0.5764197530864198
----------------------------
96 3
Train Accuracy: 0.5850158112381416
Validation Accuracy: 0.5780246913580247
----------------------------
96 4
Train Accuracy: 0.5885733398199952
Validation Accuracy: 0.5796913580246914
----------------------------
96 5
Train Accuracy: 0.5940160544879591
Validation Accuracy: 0.5818518518518518
----------------------------
96 6
Train Accuracy: 0.6074252006810995
Validation Accuracy: 0.5837654320987654
----------------------------
96 7
Train Accuracy: 0.6291960593529555
Validation Accuracy: 0.5834567901234567
----------------------------
96 8
Train Accuracy: 0.6563792264655801
Validation Accuracy: 0.5826543209876544
----------------------------
96 9
Train Accuracy: 0.6915592313305765
Validation Accuracy: 0.5871604938271605
----------------------------
96 10
Train Accuracy: 0.7422160058379956
Validation Accuracy: 0.5859876543209876
----------------------------
96 11
Train Accuracy: 0.7928423741182195
Validation Accuracy: 0.5821604938271605
----------------------------
96 12
Train Accuracy: 0.8475127706154221
Validation Accuracy: 0.5796296296296296
----------------------------
96 13
Train Accuracy: 0.89667963999027
Validation Accuracy: 0.5804938271604938
----------------------------
98 2
Train Accuracy: 0.5802724397956701
Validation Accuracy: 0.5751234567901234
----------------------------
98 3
Train Accuracy: 0.584134030649477
Validation Accuracy: 0.5783333333333334
----------------------------
98 4
Train Accuracy: 0.5884821211384091
Validation Accuracy: 0.5798765432098766
----------------------------
98 5
Train Accuracy: 0.5940160544879591
Validation Accuracy: 0.5815432098765432
----------------------------
98 6
Train Accuracy: 0.6071819508635369
Validation Accuracy: 0.5835802469135802
----------------------------
98 7
Train Accuracy: 0.6294089029433227
Validation Accuracy: 0.584320987654321
----------------------------
98 8
Train Accuracy: 0.6564704451471661
Validation Accuracy: 0.5832716049382716
----------------------------
98 9
Train Accuracy: 0.6933227925079056
Validation Accuracy: 0.5875308641975309
----------------------------
98 10
Train Accuracy: 0.741881537338847
Validation Accuracy: 0.5854938271604938
----------------------------
98 11
Train Accuracy: 0.7932072488445634
Validation Accuracy: 0.5828395061728395
----------------------------
98 12
Train Accuracy: 0.847695207978594
Validation Accuracy: 0.5796913580246914
----------------------------
98 13
Train Accuracy: 0.8981999513500365
Validation Accuracy: 0.5797530864197531
----------------------------
100 2
Train Accuracy: 0.5802420335684748
Validation Accuracy: 0.575679012345679
----------------------------
100 3
Train Accuracy: 0.584681342738993
Validation Accuracy: 0.5786419753086419
----------------------------
100 4
Train Accuracy: 0.5885125273656044
Validation Accuracy: 0.5796296296296296
----------------------------
100 5
Train Accuracy: 0.5943809292143031
Validation Accuracy: 0.5819135802469135
----------------------------
100 6
Train Accuracy: 0.6075468255898808
Validation Accuracy: 0.5840123456790124
----------------------------
100 7
Train Accuracy: 0.6295305278521041
Validation Accuracy: 0.5833950617283951
----------------------------
100 8
Train Accuracy: 0.6549501337873996
Validation Accuracy: 0.5835185185185185
----------------------------
100 9
Train Accuracy: 0.692988324008757
Validation Accuracy: 0.5867901234567902
----------------------------
100 10
Train Accuracy: 0.7419423497932377
Validation Accuracy: 0.5862962962962963
----------------------------
100 11
Train Accuracy: 0.7918997810751642
Validation Accuracy: 0.5838271604938272
----------------------------
100 12
Train Accuracy: 0.8473607394794453
Validation Accuracy: 0.5801234567901234
----------------------------
100 13
Train Accuracy: 0.8968012648990513
Validation Accuracy: 0.5795679012345679
----------------------------
102 2
Train Accuracy: 0.5796947214789588
Validation Accuracy: 0.5755555555555556
----------------------------
102 3
Train Accuracy: 0.584590124057407
Validation Accuracy: 0.5785802469135802
----------------------------
102 4
Train Accuracy: 0.5887253709559718
Validation Accuracy: 0.5794444444444444
----------------------------
102 5
Train Accuracy: 0.594837022622233
Validation Accuracy: 0.5816666666666667
----------------------------
102 6
Train Accuracy: 0.6071211384091462
Validation Accuracy: 0.584320987654321
----------------------------
102 7
Train Accuracy: 0.6287399659450256
Validation Accuracy: 0.5844444444444444
----------------------------
102 8
Train Accuracy: 0.6557406956944782
Validation Accuracy: 0.5846296296296296
----------------------------
102 9
Train Accuracy: 0.6929579177815617
Validation Accuracy: 0.5864814814814815
----------------------------
102 10
Train Accuracy: 0.7414862563853077
Validation Accuracy: 0.5858024691358025
----------------------------
102 11
Train Accuracy: 0.7927511554366334
Validation Accuracy: 0.5829012345679012
----------------------------
102 12
Train Accuracy: 0.8485769885672586
Validation Accuracy: 0.5799382716049383
----------------------------
102 13
Train Accuracy: 0.8972877645341766
Validation Accuracy: 0.5805555555555556
----------------------------
104 2
Train Accuracy: 0.5804244709316468
Validation Accuracy: 0.5753703703703704
----------------------------
104 3
Train Accuracy: 0.5843772804670396
Validation Accuracy: 0.5783333333333334
----------------------------
104 4
Train Accuracy: 0.5884213086840185
Validation Accuracy: 0.5790740740740741
----------------------------
104 5
Train Accuracy: 0.5942897105327171
Validation Accuracy: 0.5814814814814815
----------------------------
104 6
Train Accuracy: 0.6074860131354901
Validation Accuracy: 0.584320987654321
----------------------------
104 7
Train Accuracy: 0.6280710289467283
Validation Accuracy: 0.5843827160493827
----------------------------
104 8
Train Accuracy: 0.655345414740939
Validation Accuracy: 0.5841358024691358
----------------------------
104 9
Train Accuracy: 0.6925930430552177
Validation Accuracy: 0.5867283950617284
----------------------------
104 10
Train Accuracy: 0.7406652882510338
Validation Accuracy: 0.5862962962962963
----------------------------
104 11
Train Accuracy: 0.7925991243006568
Validation Accuracy: 0.5833950617283951
----------------------------
104 12
Train Accuracy: 0.8490634882023839
Validation Accuracy: 0.5801234567901234
----------------------------
104 13
Train Accuracy: 0.8970141084894186
Validation Accuracy: 0.5826543209876544
----------------------------
106 2
Train Accuracy: 0.5802116273412795
Validation Accuracy: 0.5757407407407408
----------------------------
106 3
Train Accuracy: 0.5843468742398443
Validation Accuracy: 0.578395061728395
----------------------------
106 4
Train Accuracy: 0.5889078083191438
Validation Accuracy: 0.5786419753086419
----------------------------
106 5
Train Accuracy: 0.5947762101678423
Validation Accuracy: 0.5830246913580247
----------------------------
106 6
Train Accuracy: 0.6074860131354901
Validation Accuracy: 0.5839506172839506
----------------------------
106 7
Train Accuracy: 0.6287703721722209
Validation Accuracy: 0.5848148148148148
----------------------------
106 8
Train Accuracy: 0.6560447579664316
Validation Accuracy: 0.5846296296296296
----------------------------
106 9
Train Accuracy: 0.6936572610070543
Validation Accuracy: 0.586358024691358
----------------------------
106 10
Train Accuracy: 0.7409997567501825
Validation Accuracy: 0.5869135802469135
----------------------------
106 11
Train Accuracy: 0.7929944052541961
Validation Accuracy: 0.5833333333333334
----------------------------
106 12
Train Accuracy: 0.8491547068839699
Validation Accuracy: 0.5793209876543209
----------------------------
106 13
Train Accuracy: 0.8980479202140599
Validation Accuracy: 0.5814197530864198
----------------------------
108 2
Train Accuracy: 0.5803332522500608
Validation Accuracy: 0.5758641975308642
----------------------------
108 3
Train Accuracy: 0.5837995621503284
Validation Accuracy: 0.578395061728395
----------------------------
108 4
Train Accuracy: 0.5888774020919484
Validation Accuracy: 0.5788888888888889
----------------------------
108 5
Train Accuracy: 0.5951714911213817
Validation Accuracy: 0.5823456790123457
----------------------------
108 6
Train Accuracy: 0.6075772318170761
Validation Accuracy: 0.5846296296296296
----------------------------
108 7
Train Accuracy: 0.6289832157625882
Validation Accuracy: 0.584320987654321
----------------------------
108 8
Train Accuracy: 0.6560751641936269
Validation Accuracy: 0.5839506172839506
----------------------------
108 9
Train Accuracy: 0.6940525419605935
Validation Accuracy: 0.5867901234567902
----------------------------
108 10
Train Accuracy: 0.741425443930917
Validation Accuracy: 0.5869753086419753
----------------------------
108 11
Train Accuracy: 0.7941802481148139
Validation Accuracy: 0.5834567901234567
----------------------------
108 12
Train Accuracy: 0.8502493310630017
Validation Accuracy: 0.5793209876543209
----------------------------
108 13
Train Accuracy: 0.8986864509851618
Validation Accuracy: 0.5798765432098766
----------------------------
110 2
Train Accuracy: 0.5797859401605449
Validation Accuracy: 0.5758024691358025
----------------------------
110 3
Train Accuracy: 0.5841036244222817
Validation Accuracy: 0.5781481481481482
----------------------------
110 4
Train Accuracy: 0.5889990270007297
Validation Accuracy: 0.5792592592592593
----------------------------
110 5
Train Accuracy: 0.5956883969837022
Validation Accuracy: 0.5825925925925926
----------------------------

Process finished with exit code 0

'''