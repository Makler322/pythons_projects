import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from random import random

K=100 #Кол-во точек
t=1/200 #Промежуток времени
rass=0.03 #Минимальное допустимое расстояние
X=[] #Массив иксов у всех точек
Y=[] #Массив игриков у всех точек
VX=[] #Массив скоростей по икс у всех точек
VY=[] #Массив скоростей по игрик у всех точек
Color=[] #Массив цветов
XI=[] #Массив координат х, с которым я буду работать в динамике, будем называть его массив "новых точек"
YI=[] #Массив координат х, с которым я буду работать в динамике, будем называть его массив "новых точек"


#Создаём фигуру
fig = plt.figure()
ax = plt.axes(xlim=(0, 2), ylim=(0, 2))


#Генерируем параметры точки(только 1)
def tochka():
    x=random()*2-1
    y=random()*2-1
    Vx=random()*2-1
    Vy=random()*2-1
    Color='b'
    return x,y,Vx,Vy,Color

#Изменяем координаты и цвет точки(только 1)
def delta_tochka(Vx,Vy,t,X,Y,Color,i):
    x=X[i]
    y=Y[i]
    x=x+Vx*t
    y=y+Vy*t
    if x>=2:
        x=x-2
    elif x<=0:
        x=x+2
    if y>=2:
        y=y-2
    elif y<=0:
        y=y+2
    for j in range(K):
        if Color[j]=='r':
            if 0 < ((X[j] - x) ** 2 + (Y[j] - y) ** 2) ** 0.5 <= rass:
                Color[i]= 'r'
    return x,y


#Функция выдающая нам параметры точек, требующая функция animate
def Get(L):
    XI,YI = [],[] #Анулируем значение "новых точек" и цвета
    for i in range(K):
        xi, yi = delta_tochka(VX[i], VY[i], t, X,Y,Color,i) #Высчитываем новые координаты
        X[i] = xi #Заменяем текущие координаты на новые
        Y[i] = yi
        XI.append(xi) #Заполняем массив "новых точек"
        YI.append(yi)
    return XI,YI,Color

#Функция init, используется программой
def init():
    return []



#Функция анимации, используется программой
def animate(i):
    x, y, Color = Get(i) #Получаем данные из функции Get
    Virusniki_X,Virusniki_Y,Xo,Yo=[],[],[],[]
    for i in range(K):
        if Color[i] == 'r':
            Virusniki_X.append(x[i]) #Бежим по массиву цвета, и если элемент в нём заражённый, тодобавляем координаты этого элемента в Вирусников
            Virusniki_Y.append(y[i])
        else:
            Xo.append(x[i]) #Иначе добавляем в обычный
            Yo.append(y[i])

    return ax.plot(Xo, Yo,'bo',Virusniki_X, Virusniki_Y, 'ro', ms=5)



#Часть кода, отвечающая за генерацию параметров всех точек
for j in range(K):
    x, y, Vx, Vy, color = tochka()
    X.append(x)
    Y.append(y)
    VX.append(Vx)
    VY.append(Vy)
    Color.append(color)
Color[0]='r'  #Меняем цвет у первой точки, т.к. она имеет рандомные координаты, то можно считать, что мы поменяли цвет у рандомной точки



anim = animation.FuncAnimation(fig, animate, init_func=init,frames=2000, interval=20, blit=True)
plt.show()